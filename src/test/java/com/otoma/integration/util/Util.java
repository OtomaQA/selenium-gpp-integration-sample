package com.otoma.integration.util;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Util {
    private final static String XP_POPUP_NOT_NOW_BUTTON = "//span[contains(normalize-space(),'Not now')]/..";

    private WebDriver driver;
    private WebDriverWait wait;
    private static final Logger log = Logger.getLogger(Util.class.getName());

    public Util(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public String monthNumberToString(String monthNumber) {
        String strNoZero = monthNumber.replace("0", "");
        int month = Integer.parseInt(strNoZero);
        String result = "";

        switch (month) {
            case 1: result = "JAN"; break;
            case 2: result = "FEB"; break;
            case 3: result = "MAR"; break;
            case 4: result = "APR"; break;
            case 5: result = "MAY"; break;
            case 6: result = "JUN"; break;
            case 7: result = "JUL"; break;
            case 8: result = "AUG"; break;
            case 9: result = "SEP"; break;
            case 10: result = "OCT"; break;
            case 11: result = "NOV"; break;
            case 12: result = "DEC"; break;
        }
        return result;
    }

    public void handlePopupIfExists() {
        String xpath = XP_POPUP_NOT_NOW_BUTTON;

        try {
            wait.withTimeout(Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            WebElement notNowBtn = driver.findElement(By.xpath(xpath));
            if (notNowBtn.isDisplayed()) {
                notNowBtn.click();
                log.info("Clicked a popup's Not Now button");
            }
        } catch (Exception e) {
            log.info("No popup to handle");
        }
    }

    public boolean waitForNewWindow(int initCount) {
        boolean countChanged;

        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);


        countChanged = fluentWait.until(driver1 -> {
            int currCount = driver1.getWindowHandles().size();
            log.info(String.format("FluentWait -- Current = %d / Init = %d", currCount, initCount));
            boolean res = currCount != initCount;

            return res;
        });

        return countChanged;
    }

    public void trySwitchToWindow(String windowHandle) {
        try {
            Set<String> allHandles = driver.getWindowHandles();
            if (allHandles.size() > 1) {
                driver.switchTo().window(windowHandle);
                log.info(String.format("Switched to: %s", windowHandle));
            } else {
                log.info("Only one window handle - not switching");
            }
        } catch (Exception e) {
            log.log(Level.INFO, "Error occurred", e);
        }
    }

    public String getCurrentDateString() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy_HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}
