package com.otoma.integration.gpp;

import com.otoma.integration.util.Util;
import com.otoma.scripting.ScreenshotTaker;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SearchHandler {
    private final static String ID_SEARCH_FIELD = "dh-search-box-input";
    private final static String ID_SEARCH_BUTTON = "search-box-button";
    private final static String XP_SEARCH_RESULTS_COUNT = "//span[contains(@class,'count') and string-length(text()) > 0]";
    private final static String XP_SEARCH_RESULT_STATUS_BY_INDEX = "(//*[contains(@id,'P_MSG_STS')]/div/span)[%d]";
//    private final static String XP_SEARCH_RESULT_STATUS_BY_INDEX = "(//td[contains(@class,'P_INSTR_ID')])[%d]";
    private final static String XP_SEARCH_RESULT_INFO_HEADER = "//div[contains(normalize-space(),'Basic attributes')]";

    private static final Logger log = Logger.getLogger(SearchHandler.class.getName());

    private final ScreenshotTaker screenshotTaker;
    private WebDriver driver;
    private WebDriverWait wait;
    private Util util;

    public SearchHandler(WebDriver driver, WebDriverWait wait, ScreenshotTaker screenshotTaker) {
        this.driver = driver;
        this.wait = wait;
        this.util = new Util(this.driver, this.wait);
        this.screenshotTaker = screenshotTaker;
    }

    public boolean openSearchResultByIndex(int resultIndex, String mainWindowHandle) {
        String xpath = String.format(XP_SEARCH_RESULT_STATUS_BY_INDEX, resultIndex);
        boolean result;

        String targetHandle = "";
        int handleCount = driver.getWindowHandles().size();
        util.handlePopupIfExists();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).click();

        if (util.waitForNewWindow(handleCount)) {
            Set<String> allHandles = driver.getWindowHandles();

            for (String h : allHandles) {
                if (!mainWindowHandle.equalsIgnoreCase(h)) {
                    targetHandle = h;
                    util.trySwitchToWindow(h);
                }
            }
        } else {
            log.warning("Failed on waitForNewWindow()");
        }
        result = isSearchResultHeaderDisplayed(false, targetHandle);

        return result;
    }

    private boolean isSearchResultHeaderDisplayed(boolean isRetry, String targetWindowHandle) {
        String xpath = XP_SEARCH_RESULT_INFO_HEADER;
        boolean result = false;

        try {
            if (isRetry && (targetWindowHandle != null && !targetWindowHandle.isEmpty())) {
                util.trySwitchToWindow(targetWindowHandle);
            }
            wait.withTimeout(Duration.ofSeconds(30)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            result = driver.findElement(By.xpath(xpath)).isDisplayed();
        } catch (Exception e) {
            log.log(Level.INFO, "Error occurred", e);
            if (!isRetry) {
                result = isSearchResultHeaderDisplayed(true, targetWindowHandle);
            } else {
                this.screenshotTaker.screenshot("searchResult_Error");
            }
        }
        finally {
            log.info(String.format("Search Result Header - isDisplayed = %b", result));
            return result;
        }
    }

    public boolean search(String searchTerm) {
        boolean result;
        setSearchTerm(searchTerm);
        clickSearchButton();

        util.handlePopupIfExists();
        int resCount = getSearchResultsCount();

        result = resCount > 0;
        return result;
    }

    private int getSearchResultsCount() {
        int count = 0;
        String xpath = XP_SEARCH_RESULTS_COUNT;
        String strRemove = "Results";

        wait.withTimeout(Duration.ofSeconds(20)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        String txt = driver.findElement(By.xpath(xpath)).getText();
        if (txt != null && !txt.isEmpty()) {
            txt = txt.replace(strRemove, "").trim();
            count = Integer.parseInt(txt);
            log.info(String.format("Parsed '%s' to %d", txt, count));
        }
        log.info(String.format("Returning %d results", count));
        return count;
    }

    private void setSearchTerm(String searchTerm) {
        String id = ID_SEARCH_FIELD;
        Actions act = new Actions(driver);

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        act.moveToElement(driver.findElement(By.id(id))).click().sendKeys(searchTerm).build().perform();

        log.info(String.format("Set search term: '%s'", searchTerm));
    }

    private void clickSearchButton() {
        String id = ID_SEARCH_BUTTON;

        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
            driver.findElement(By.id(id)).click();

            log.info("Clicked the Search button");
        } catch (ElementClickInterceptedException e) {
            log.info("Click intercepted; trying to handle pop-up");
            util.handlePopupIfExists();
            clickSearchButton();
        }
    }
}
