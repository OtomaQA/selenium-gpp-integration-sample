package com.otoma.integration.gpp;

import com.otoma.integration.util.Util;
import com.otoma.scripting.ScreenshotTaker;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.Set;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class LoginHandler {
    private final static String ID_USERNAME_INPUT = "signin-username-field";
    private final static String ID_PASSWORD_INPUT = "signin-password-field";
    private final static String ID_LOGIN_BUTTON = "signin-login-button";
    private final static String ID_USER_BUTTON = "btn-user-dropdown";
//    private final static String XP_USER_BUTTON = "//button[contains(@id,'btn-user-dropdown')]";
    private final static String ID_LOGOUT_BUTTON = "logout-btn";
//    private final static String XP_LOGOUT_BUTTON = "//span[contains(@class,'mdi-logout')]/../..";
    private final static String XP_LOGIN_ERROR = "//div[contains(@class, 'danger')]";
    private final static String XP_LOGIN_SUCCESS_MESSAGE = "//div[contains(@class,'dh-notification')]";

    private static final Logger log = Logger.getLogger(LoginHandler.class.getName());

    private WebDriver driver;
    private WebDriverWait wait;
    private Util util;
    private Actions act;
    private final ScreenshotTaker screenshotTaker;

    public LoginHandler(WebDriver driver, WebDriverWait wait, ScreenshotTaker screenshotTaker) {
        this.driver = driver;
        this.wait = wait;
        this.util = new Util(this.driver, this.wait);
        this.act = new Actions(this.driver);
        this.screenshotTaker = screenshotTaker;
    }

    private boolean isLoginPageDisplayed(boolean longWait) {
        boolean result = false;
        String id = ID_LOGIN_BUTTON;

        try {
            int waitSec = longWait ? 15 : 1;
            wait.withTimeout(Duration.ofSeconds(waitSec)).until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
            result = driver.findElement(By.id(id)).isDisplayed();
            log.info(String.format("Login page is %sdisplayed", result ? "" : "not "));
        } catch (Exception e) {
            log.info("Login Page is not displayed");
        }
        return result;
    }

    public boolean switchToParentWindow() {
        Set<String> winHandles = driver.getWindowHandles();
        int winCount = winHandles.size();
        String id = ID_USER_BUTTON;
        log.log(INFO, String.format("WinCount = %d", winCount));

        for (String winHandle : winHandles) {
            try {
                util.trySwitchToWindow(winHandle);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
                return true;
            } catch (Exception e) {
                log.log(INFO, String.format("WinHandle is not the Main Window: '%s'", winHandle));
            }
        }
        log.log(WARNING, String.format("Main Window not found! (%d windows)", winCount));
        return false;
    }

    private void handleLoginSuccessMessage() {
        String xpath = XP_LOGIN_SUCCESS_MESSAGE;

        try {
            wait.withTimeout(Duration.ofSeconds(5)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            driver.findElement(By.xpath(xpath)).click();

            log.info("Clicked the Login Success message");
        } catch (Exception e) {
            log.log(WARNING, "Could not handle Login Success Message", e);
        }
    }

    public boolean tryToLogout() {
        boolean result = false;
        String id = ID_USER_BUTTON;

        if (!isLoginPageDisplayed(false)) {
            switchToParentWindow();
            wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
            driver.findElement(By.id(id)).click();
            log.info("Opened top menu");

            id = ID_LOGOUT_BUTTON;
            wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
            driver.findElement(By.id(id)).click();
            log.info("Clicked to Logout");
            if (waitForLogout()) {
                result = isLoginPageDisplayed(false);
            }
        } else {
            log.info("Already logged-out");
            result = true;
        }
        return result;
    }

    public boolean tryToLogin(String username, String password) {
        boolean result = false;

        if (isLoginPageDisplayed(true)) {
            setUsername(username);
            setPassword(password);
            clickLoginButton();
            if (!isLoginErrorDisplayed()) {
                result = waitForLogin();
                handleLoginSuccessMessage();
                util.handlePopupIfExists();
            }
        } else {
            screenshotTaker.screenshot("LoginFail");
        }

        return result;
    }

    private boolean waitForLogin() {
        boolean result;

        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);


        result = fluentWait.until(driver1 -> {
            boolean res = !isLoginPageDisplayed(false);
            log.info(String.format("FluentWait -- isLoggedIn = %b", res));
            return res;
        });
        return result;
    }

    private boolean waitForLogout() {
        boolean result;

        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(15))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);


        result = fluentWait.until(driver1 -> {
            boolean res = isLoginPageDisplayed(false);
            log.info(String.format("FluentWait -- isLoggedOut = %b", res));
            return res;
        });
        return result;
    }

    private boolean isLoginErrorDisplayed() {
        boolean result = false;
        String xpath = XP_LOGIN_ERROR;

        try {
            wait.withTimeout(Duration.ofSeconds(3)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            result = driver.findElement(By.xpath(xpath)).isDisplayed();

            log.info(String.format("Login Error %s", result ? "is displayed - check credentials" : "is not displayed"));
        } catch (Exception e) {
            log.info("No Login Error");
        }
        return result;
    }

    private void setUsername(String username) {
        String id = ID_USERNAME_INPUT;
        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        driver.findElement(By.id(id)).sendKeys(username);
        log.info(String.format("Set Username: '%s'", username));
    }

    private void setPassword(String password) {
        String id = ID_PASSWORD_INPUT;
        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        driver.findElement(By.id(id)).sendKeys(password);
        String hidden = "*****";
        log.info(String.format("Set Password: '%s'", hidden));
    }

    private void clickLoginButton() {
        String id = ID_LOGIN_BUTTON;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        act.moveToElement(driver.findElement(By.id(id))).click().build().perform();

        log.info("Clicked the Login button");
    }
}
