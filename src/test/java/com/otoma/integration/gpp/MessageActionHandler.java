package com.otoma.integration.gpp;

import com.otoma.scripting.ScreenshotTaker;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.otoma.integration.util.Util;
import java.time.Duration;
import java.util.Set;
import java.util.logging.Logger;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class MessageActionHandler {
    private final static String ID_NOTES_TAB = "PN_Notes_btn";
    private final static String ID_FX_TAB = "Msg_Rate_btn";
    private final static String ID_GET_RATE_BUTTON = "MSG_RATES_GRID_BTNS_PANEL.getRate";
    private final static String XP_RATE_ACTION_BUTTON ="//span[@action='B_ACT_CLOSE_%s_%s_RATE']";
    private final static String ID_ADD_NOTE_BUTTON = "MSG_NOTES_GRID_BTNS_PANEL.addRowBtn";
    private final static String ID_NEW_NOTE_TEXT_AREA = "MSG_NOTES_EDT_FORM.F_MSG_NOTES_TEXT";
    private final static String ID_APPLY_NEW_NOTE_BUTTON = "MSG_NOTES_EDT_FORM.applyBtn";
    private final static String XP_EXISTING_NOTE_BY_TEXT = "//td[@name='MSG_NOTES_EDT_FORM.F_MSG_NOTES_TEXT' and normalize-space()='%s']";
    private final static String XP_SAVE_POPUP = "//div[@role='dialog' and contains(normalize-space(),'Do you want to save?')]";
    private final static String XP_SAVE_POPUP_SAVE_BUTTON = "//div[@role='dialog' and contains(normalize-space(),'Do you want to save?')]//div[contains(@class,'buttonpane')]//button[contains(@text,'Save')]";
    private final static String XP_MSG_ACTION_BUTTON_BY_TEXT = "//button[contains(@id,'PN_BUTTON') or contains(@id,'PN_COMPEX')]//span[contains(normalize-space(),'%s')]/..";
    private final static String XP_FIRST_MSG_ACTION_BUTTON = "(//button[contains(@id,'PN_BUTTON_') and contains(@class,'actionButton')])[1]";

    private static final Logger log = Logger.getLogger(MessageActionHandler.class.getName());

    private WebDriver driver;
    private WebDriverWait wait;
    private Util util;
    private Actions act;
    private final ScreenshotTaker screenshotTaker;

    public MessageActionHandler(WebDriver driver, WebDriverWait wait, ScreenshotTaker screenshotTaker) {
        this.driver = driver;
        this.wait = wait;
        this.util = new Util(this.driver, this.wait);
        this.act = new Actions(this.driver);
        this.screenshotTaker = screenshotTaker;
    }

    private void scrollToFirstActionButton() {
        String xpath = XP_FIRST_MSG_ACTION_BUTTON;

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        act.moveToElement(driver.findElement(By.xpath(xpath))).sendKeys("").build().perform();
        log.info("Scrolled to first action button");
    }

    public boolean clickActionButton(String msgAction, boolean isRetry) {
        String xpath = String.format(XP_MSG_ACTION_BUTTON_BY_TEXT, msgAction);
        boolean result = false;

        try {
            wait.withTimeout(Duration.ofSeconds(2)).until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            scrollToFirstActionButton();
            this.screenshotTaker.screenshot(String.format("Action_%s", msgAction.replaceAll("/","-")));
            act.moveToElement(driver.findElement(By.xpath(xpath))).click().build().perform();

            log.info(String.format("Clicked the %s action button", msgAction));
            result = handlePopupIfExists();
        }
        catch (Exception e) {
            log.log(INFO, String.format("Failed on msgAction '%s'", msgAction), e);
            if (!isRetry) {
                scrollToFirstActionButton();
                this.screenshotTaker.screenshot(String.format("ActionFail_%s", msgAction.replaceAll("/", "-")));
                log.warning("Trying to Exit...");
                clickActionButton("Exit", true);
            } else {
                log.log(WARNING, "Error occurred", e);
            }
        }
        return result;
    }

    private boolean handlePopupIfExists() {
        boolean success;
        if (doesPopupExist()) {
            String xpath = XP_SAVE_POPUP_SAVE_BUTTON;

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            driver.findElement(By.xpath(xpath)).click();
            log.info("Clicked the Save Popup's SAVE button");
            success = waitForSavePopupToClose();
        } else {
            log.info("No popup to handle");
            success = true;
        }
        return success;
    }

    private boolean waitForSavePopupToClose() {
        boolean closed;
        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(15))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);

        closed = fluentWait.until(driver1 -> {
            boolean res = !doesPopupExist();
            return res;
        });

        return closed;
    }

    private boolean doesPopupExist() {
        String xpath = XP_SAVE_POPUP;
        boolean exists = false;

        try {
            wait.withTimeout(Duration.ofSeconds(3)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            exists = driver.findElement(By.xpath(xpath)) != null;
            log.info(String.format("doesPopupExist -- Got value: %b", exists));
        }
        catch (Exception e) {
            log.info("Popup does not exist");
        }
        return exists;
    }

    private boolean doesNoteExist(String noteText) {
        String xpath = String.format(XP_EXISTING_NOTE_BY_TEXT, noteText);
        boolean exists;

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        exists = driver.findElement(By.xpath(xpath)) != null;
        log.info(String.format("doesNoteExist -- '%s' %s", noteText, exists ? "exists" : "does not exist"));

        return exists;
    }

    public String generateMessageNote(String customText) {
        boolean customize = customText != null && !customText.isEmpty();
        String pattern = customize ? "OTOMA_%s_%s" : "OTOMA_%s";
        return customize ? String.format(pattern, customText, util.getCurrentDateString()) : String.format(pattern, util.getCurrentDateString());
    }

    public boolean getRate() {
        try {
            clickFXTab();
            clickGetRateButton();
            return true;
        } catch (Exception e) {
            log.log(WARNING, "Error occurred", e);
            return false;
        }
    }

    private boolean displayMessageWindow() {
        Set<String> winHandles = driver.getWindowHandles();
        int winCount = winHandles.size();
        log.log(INFO, String.format("WinCount = %d", winCount));

        String xpath = String.format(XP_MSG_ACTION_BUTTON_BY_TEXT, "Exit");

        for (String winHandle : winHandles) {
            try {
                util.trySwitchToWindow(winHandle);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
                return true;
            }
            catch (Exception e) {
                log.log(INFO, String.format("WinHandle is not the Message Window: '%s'", winHandle));
            }
        }
        log.log(WARNING, String.format("Message Window not found! (%d windows)", winCount));
        return false;
    }

    private boolean displayRatePopup(String xpath) {
        Set<String> winHandles = driver.getWindowHandles();
        int winCount = winHandles.size();
        log.log(INFO, String.format("WinCount = %d | XPath = %s", winCount, xpath));

        for (String winHandle : winHandles) {
            try {
                util.trySwitchToWindow(winHandle);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
                return true;
            } catch (Exception e) {
                log.log(INFO, String.format("WinHandle is not the Rate Popup: '%s'", winHandle));
            }
        }
        log.log(WARNING, String.format("Rate Popup not found! (%d windows)", winCount));
        return false;
    }

    public boolean rateAction(boolean approve) {
        String action = approve ? "APPROVE" : "REJECT";
        String type = approve ? "MSG" : "DIALOG";
        String xpath = String.format(XP_RATE_ACTION_BUTTON, type, action);
        if (displayRatePopup(xpath)) {
            try {
                int expectedWindowCount = driver.getWindowHandles().size() - 1;
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
                act.moveToElement(driver.findElement(By.xpath(xpath))).sendKeys("").build().perform();
                screenshotTaker.screenshot("Rate_Window");
                act.moveToElement(driver.findElement(By.xpath(xpath))).click().build().perform();
                //TODO: Fix text
                log.log(INFO, String.format("Clicked the %s button", action));
                wait.until(ExpectedConditions.numberOfWindowsToBe(expectedWindowCount));
                return true;
            } catch (Exception e) {
                log.log(WARNING, "Error occurred", e);
                return false;
            }
        } else {
            log.log(WARNING, "Rate Popup is not displayed!");
            return false;
        }
    }

    public boolean populateMessageNotes(String noteText)
    {
        boolean result;
        if (displayMessageWindow()) {
            clickNotesTab();
            clickAddNewNoteButton();
            setNewNoteText(noteText);
            clickApplyNewNoteButton();
            result = doesNoteExist(noteText);
        } else {
            log.log(WARNING, "Message Window is not displayed - can't add Note");
            result = false;
        }
        return result;
    }

    private void clickApplyNewNoteButton() {
        String id = ID_APPLY_NEW_NOTE_BUTTON;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        act.moveToElement(driver.findElement(By.id(id))).click().build().perform();
        log.info("Clicked the New Note Apply button");
    }

    private void setNewNoteText(String newNoteText) {
        String id = ID_NEW_NOTE_TEXT_AREA;

        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
            driver.findElement(By.id(id)).clear();
            driver.findElement(By.id(id)).sendKeys(newNoteText);
            log.info(String.format("Set New Note Text: '%s'", newNoteText));
        } catch (UnhandledAlertException f) {
            try {
                Alert a = driver.switchTo().alert();
                log.info(String.format("Alert text: %s", a.getText()));
                a.accept();
                log.info("Accepted alert; trying again");
                setNewNoteText(newNoteText);
            } catch (Exception e) {
                log.log(INFO, "Error occurred", e);
            }
        }
    }

    private void clickAddNewNoteButton() {
        String id = ID_ADD_NOTE_BUTTON;

        try {
            wait.withTimeout(Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By.id(id)));
            act.moveToElement(driver.findElement(By.id(id))).click().build().perform();
            log.info("Clicked the Add Note button");
        }
        catch (UnhandledAlertException f) {
            try {
                wait.until(ExpectedConditions.alertIsPresent());
                Alert a = driver.switchTo().alert();
                log.info(String.format("Alert text: %s", a.getText()));
                a.accept();
                log.info("Accepted alert");
                clickAddNewNoteButton();
            } catch (Exception e) {
                log.log(INFO, "Error occurred", e);
            }
        }
    }

    private void clickGetRateButton() {
        String id = ID_GET_RATE_BUTTON;

        try {
            wait.withTimeout(Duration.ofSeconds(3)).until(ExpectedConditions.elementToBeClickable(By.id(id)));
            act.moveToElement(driver.findElement(By.id(id))).click().build().perform();
            log.info("Clicked the Get Rate button");
        } catch (Exception e) {
            log.log(WARNING, "Error occurred", e);
        }
    }

    private void clickNotesTab() {
        String id = ID_NOTES_TAB;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        act.moveToElement(driver.findElement(By.id(id))).click().build().perform();
        log.info("Clicked the Notes button");
    }

    private void clickFXTab() {
        String id = ID_FX_TAB;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        act.moveToElement(driver.findElement(By.id(id))).click().build().perform();
        log.info("Clicked the FX button");
    }
}
