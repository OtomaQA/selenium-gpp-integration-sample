package com.otoma.integration.gpp;

import com.otoma.integration.util.Util;
import com.otoma.scripting.ScreenshotTaker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.logging.Logger;

public class PaymentHandler {
    private final static String XP_CREATE_PAYMENT_BUTTON = "//button[@data-ng-click='createPayment()']";
    private final static String ID_PAYMENT_TYPE_SELECT = "create-payment-select-type";
    private final static String ID_PAYMENT_TYPE_SUBMIT_BUTTON = "create-payment-submit-button";
    private final static String CSS_NEW_PAYMENT_FORM = "app-create-customer-message-main";
    private final static String XP_INPUT_SUGGESTION_BY_TEXT = "//span[@class='mat-option-text' and normalize-space()='%s']";
    private final static String ID_SUBMIT_NEW_PAYMENT_BUTTON = "message-button-submit";
    private final static String ID_CLOSE_NEW_PAYMENT_BUTTON = "message-button-close";
    private final static String ID_DISCARD_BUTTON = "exit-and-discard-modal-discard-button";
    private final static String ID_SAVE_BUTTON = "exit-and-discard-modal-save-button";
    private final static String XP_NEW_PAYMENT_SUCCESS_MESSAGE = "//div[@ng-bind-html='message' and normalize-space()='The Message was successfully submitted']";
    // Payment Information inputs
    private final static String XP_INPUT_DEPARTMENT = "//app-dh-select[@id='P_DEPARTMENT']//input";
    private final static String XP_INPUT_OFFICE = "//app-dh-select[@id='P_OFFICE']//input";
    private final static String XP_INPUT_INSTRUCTION_ID = "//app-dh-input[@id='X_INSTR_ID']//input";
    // Settlement Instructions inputs
    private final static String XP_INPUT_CURRENCY = "//app-dh-select[@id='OX_STTLM_CCY']//input";
    private final static String XP_INPUT_AMOUNT = "//app-dh-input[@id='OX_STTLM_AMT']//input";
    private final static String XP_INPUT_VALUE_DATE = "//app-dh-datepicker[@id='OX_STTLM_DT_1B']//input";
    private final static String XP_INPUT_VALUE_DATEPICKER = "//app-dh-datepicker[@id='OX_STTLM_DT_1B']//mat-datepicker-toggle";
    private final static String XP_INPUT_VALUE_DATE_YEAR_SELECTOR = "//button[contains(@class, 'mat-calendar-period-button')]";
    // YEAR + Day (full date)
    private final static String XP_INPUT_VALUE_DATE_ITEM = "//td[@aria-label='%s']";
    private final static String XP_INPUT_VALUE_DATE_MONTH = "//td//div[normalize-space()='%s']";
    private final static String XP_INPUT_CHARGES_BEARER = "//app-dh-select[@id='OX_CHRG_BR']//input";
    // Debtor + Creditor Account Details
    private final static String XP_INPUT_FILTER_BY_ACCOUNT_NUMBER = "//input[@id='ACCOUNTS-ACC_NO']";
    private final static String XP_ROW_BY_ACCOUNT_NUMBER = "//div[contains(@id, 'ACCOUNTS-ACC_NO')]//span[normalize-space()='%s']";
    private final static String XP_INPUT_DEBTOR_ACCOUNT_BUTTON = "//app-dh-drilldown[@id='X_DBTR_ACCT_ID']//button";
    private final static String XP_INPUT_CREDITOR_ACCOUNT_BUTTON = "//app-dh-drilldown[@id='X_CDTR_ACCT_ID']//button";
    // Debtor + Creditor Agent Details
    private final static String XP_INPUT_FILTER_BY_PARTY_CODE = "//input[@id='%s']";
    private final static String FILTER_BY_PARTY_CODE_BIC = "CUSTOMRS-CUST_CODE";
    private final static String FILTER_BY_PARTY_CODE_BBAN = "NCC-CUST_CODE";
    private final static String XP_ROW_BY_PARTY_CODE = "//div[contains(@id, '%s')]//span[normalize-space()='%s']";
    private final static String XP_ROW_VALUE_BY_PARTY_CODE = "//..//..//..//div[contains(@id, '%s')]//span";
    private final static String BIC_FIELD_NAME = "CUSTOMRS-SWIFT_ID-0";
    private final static String BBAN_FIELD_NAME = "NCC-NCC_CODE-0";
    private final static String XP_INPUT_AGENT_ID_TYPE = "//app-dh-select[@id='X_%s_AGT_CLR_SYS_CD']//input";
    private final static String XP_INPUT_AGENT_BUTTON = "//app-dh-drilldown[@id='X_%s_AGT_%s_2AND']//button";
    private final static String XP_INPUT_AGENT_FIELD = "//app-dh-drilldown[@id='X_%s_AGT_%s_2AND']//input";
    private final static String AGENT_INPUT_BIC = "BIC";
    private final static String AGENT_INPUT_BBAN = "ID";
    private final static String DEBTOR = "DBTR";
    private final static String CREDITOR = "CDTR";

    private static final Logger log = Logger.getLogger(PaymentHandler.class.getName());

    private final ScreenshotTaker screenshotTaker;
    private WebDriver driver;
    private WebDriverWait wait;
    private Util util;
    private Actions act;

    public PaymentHandler(WebDriver driver, WebDriverWait wait, ScreenshotTaker screenshotTaker) {
        this.driver = driver;
        this.wait = wait;
        this.util = new Util(this.driver, this.wait);
        this.screenshotTaker = screenshotTaker;
        this.act = new Actions(this.driver);
    }

    public void closePaymentScreen(boolean saveChanges) {
        String btnID = ID_CLOSE_NEW_PAYMENT_BUTTON;
        String optID = saveChanges ? ID_SAVE_BUTTON : ID_DISCARD_BUTTON;
        String opt = saveChanges ? "Save" : "Discard";

        wait.until(ExpectedConditions.elementToBeClickable(By.id(btnID)));
        driver.findElement(By.id(btnID)).click();
        log.info("Clicked the CLOSE button");

        wait.until(ExpectedConditions.elementToBeClickable(By.id(optID)));
        driver.findElement(By.id(optID)).click();
        log.info(String.format("Clicked the %s button", opt));
    }

    public boolean createNewPayment(String paymentType, String department, String office, String instructionId, String currency, String amount, String valueDate,
                                    String chargesBearer, String debtorAccount, String debtorAgentIdType, String debtorAgentPartyCode, String creditorAccount, String creditorAgentIdType,
                                    String creditorAgentPartyCode) throws Exception {
        boolean success = false;
        int waitForFormLoadInSec = 5;
        int waitForNewPaymentInSec = 15;
        log.info(String.format("Creating new payment: \nPaymentType: %s | Department: %s | Office: %s | InstructionId: %s | Currency: %s | Amount: %s | ValueDate: %s | " +
                "ChargesBearer: %s | DebtorAccount: %s | DebtorAgentIDType: %s | DebtorAgentPartyCode: %s | CreditorAccount: %s | CreditorAgentIDType: %s | CreditorAgentPartyCode: %s",
                paymentType, department, office, instructionId, currency, amount, valueDate, chargesBearer,
                debtorAccount, debtorAgentIdType, debtorAgentPartyCode, creditorAccount, creditorAgentIdType, creditorAgentPartyCode));

        clickCreateNewPayment();
        selectPaymentType(paymentType);
        submitNewPaymentType();
        if (isNewPaymentFormDisplayed(waitForFormLoadInSec)) {
            setDepartment(department);
            setOffice(office);
            setInstructionId(instructionId);
            setCurrency(currency);
            setAmount(amount);
//            setValueDate(valueDate);
            selectValueDate(valueDate);
            setChargesBearer(chargesBearer);
            populateAccount(true, debtorAccount);
            populateAgent(true, debtorAgentIdType, debtorAgentPartyCode);
            populateAccount(false, creditorAccount);
            populateAgent(false, creditorAgentIdType, creditorAgentPartyCode);
            clickSubmitNewPayment();
            success = waitForNewPaymentSuccessMessage(waitForNewPaymentInSec);
        } else {
            throw new Exception(String.format("Timed-out on New Payment Form (%d sec.)", waitForFormLoadInSec));
        }
        log.info("Returning: " + success);
        return success;
    }

    private boolean waitForNewPaymentSuccessMessage(int waitForSuccessInSec) {
        String xpath = XP_NEW_PAYMENT_SUCCESS_MESSAGE;
        Duration waitFor = Duration.ofSeconds(waitForSuccessInSec);

        try {
            wait.withTimeout(waitFor).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            log.info("New Payment Success Message found - returning True");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void clickSubmitNewPayment() {
        String id = ID_SUBMIT_NEW_PAYMENT_BUTTON;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        driver.findElement(By.id(id)).click();
        log.info("Clicked the Submit New Payment button");
    }

    private void clickCreateNewPayment() {
        String xpath = XP_CREATE_PAYMENT_BUTTON;

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).click();
        log.info("Clicked the Create a Payment button");
    }

    private void selectPaymentType(String paymentType) {
        String id = ID_PAYMENT_TYPE_SELECT;

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
        Select sel = new Select(driver.findElement(By.id(id)));
        sel.selectByVisibleText(paymentType);
        log.info("Selected Payment Type: " + paymentType);
    }

    private void submitNewPaymentType() {
        String id = ID_PAYMENT_TYPE_SUBMIT_BUTTON;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        driver.findElement(By.id(id)).click();
        log.info("Clicked Submit New Payment Type");
    }

    private boolean isNewPaymentFormDisplayed(int waitForSec) {
        String selector = CSS_NEW_PAYMENT_FORM;

        try {
            wait.withTimeout(Duration.ofSeconds(waitForSec)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(selector)));
            log.info("New Payment Form found - returning True");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void setDepartment(String department) {
        String xpath = XP_INPUT_DEPARTMENT;
        String suggXPath = String.format(XP_INPUT_SUGGESTION_BY_TEXT, department);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).clear();
        driver.findElement(By.xpath(xpath)).sendKeys(department);
        log.info("Set Department: " + department);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(suggXPath)));
        driver.findElement(By.xpath(xpath)).click();
        log.info("Selected suggested Department: " + department);
    }

    private void setOffice(String office) {
        String xpath = XP_INPUT_OFFICE;

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        boolean isEnabled = driver.findElement(By.xpath(xpath)).isEnabled();
        log.info("Office input is enabled: " + isEnabled);
        if (isEnabled) {
            String suggXPath = String.format(XP_INPUT_SUGGESTION_BY_TEXT, office);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            driver.findElement(By.xpath(xpath)).clear();
            driver.findElement(By.xpath(xpath)).sendKeys(office);
            log.info("Set Office: " + office);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(suggXPath)));
            driver.findElement(By.xpath(suggXPath)).click();
            log.info("Selected suggested Office: " + office);
        }
    }

    private void setInstructionId(String instrId) {
        String xpath = XP_INPUT_INSTRUCTION_ID;

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).sendKeys(instrId);
        log.info("Set InstructionId: " + instrId);
    }

    private void setCurrency(String currency) {
        String xpath = XP_INPUT_CURRENCY;
        String suggXPath = String.format(XP_INPUT_SUGGESTION_BY_TEXT, currency);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).clear();
        driver.findElement(By.xpath(xpath)).sendKeys(currency);
        log.info("Set SettlementInstruction.Currency: " + currency);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(suggXPath)));
        driver.findElement(By.xpath(suggXPath)).click();
        log.info("Selected suggested SettlementInstruction.Currency: " + currency);
    }

    private void setAmount(String amount) {
        String xpath = XP_INPUT_AMOUNT;

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).sendKeys(amount);
        log.info("Set SettlementInstruction.Amount: " + amount);
    }

    private void setValueDate(String valueDate) {
        String xpath = XP_INPUT_VALUE_DATE;

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).click();
        driver.findElement(By.xpath(xpath)).clear();
        driver.findElement(By.xpath(xpath)).sendKeys(valueDate);
        log.info("Set SettlementInstruction.ValueDate: " + valueDate);
    }

    private void selectValueDate(String valueDate) {
        log.info("Working on: " + valueDate);
        String[] split = valueDate.split("/");
        if (split.length > 0) {
            // DD/MM/YYYY
            String day = split[0];
            String month = split[1];
            String monthName = util.monthNumberToString(month);
            String year = split[2];

            String pickerXPath = XP_INPUT_VALUE_DATEPICKER;
            String yearButtonXPath = XP_INPUT_VALUE_DATE_YEAR_SELECTOR;
            String yearItemXPath = String.format(XP_INPUT_VALUE_DATE_ITEM, year);
            String monthXPath = String.format(XP_INPUT_VALUE_DATE_MONTH, monthName);
            String dayXPath = String.format(XP_INPUT_VALUE_DATE_ITEM, valueDate);
            String inputXPath = XP_INPUT_VALUE_DATE;

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(pickerXPath)));
            act.moveToElement(driver.findElement(By.xpath(pickerXPath))).click().build().perform();
            log.info("Clicked the DatePicker button");

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(yearButtonXPath)));
            act.moveToElement(driver.findElement(By.xpath(yearButtonXPath))).click().build().perform();
            log.info("Clicked the Year Selector button");

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(yearItemXPath)));
            act.moveToElement(driver.findElement(By.xpath(yearItemXPath))).click().build().perform();
            log.info("Selected Year: " + year);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(monthXPath)));
            act.moveToElement(driver.findElement(By.xpath(monthXPath))).click().build().perform();
            log.info("Selected Month: " + monthName);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(dayXPath)));
            act.moveToElement(driver.findElement(By.xpath(dayXPath))).click().build().perform();
            log.info("Selected Day: " + day);

            // Compare actual & expected
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(inputXPath)));
            String val = driver.findElement(By.xpath(inputXPath)).getAttribute("value").trim();
            log.info(String.format("Expected: %s / Actual: %s", valueDate, val));
        }
    }

    private void setChargesBearer(String chargesBearer) {
        String xpath = XP_INPUT_CHARGES_BEARER;
        String suggXPath = String.format(XP_INPUT_SUGGESTION_BY_TEXT, chargesBearer);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).clear();
        driver.findElement(By.xpath(xpath)).sendKeys(chargesBearer);
        log.info("Set SettlementInstruction.ChargesBearer: " + chargesBearer);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(suggXPath)));
        driver.findElement(By.xpath(suggXPath)).click();
        log.info("Selected suggested SettlementInstruction.ChargesBearer: " + chargesBearer);
    }

    private void populateAccount(boolean isDebtor, String accountNumber) {
        Duration openTimeout = Duration.ofSeconds(10);
        Duration searchTimeout = Duration.ofSeconds(5);
        String threeDotsXPath = isDebtor ? XP_INPUT_DEBTOR_ACCOUNT_BUTTON : XP_INPUT_CREDITOR_ACCOUNT_BUTTON;
        String filterByXpath = XP_INPUT_FILTER_BY_ACCOUNT_NUMBER;
        String rowXPath = String.format(XP_ROW_BY_ACCOUNT_NUMBER, accountNumber);
        String entityName = isDebtor ? "DebtorAccount" : "CreditorAccount";

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(threeDotsXPath)));
        driver.findElement(By.xpath(threeDotsXPath)).click();
        log.info(String.format("Clicked %s's three-dots button...", entityName));

        wait.withTimeout(openTimeout).until(ExpectedConditions.elementToBeClickable(By.xpath(filterByXpath)));
        driver.findElement(By.xpath(filterByXpath)).clear();
        act.moveToElement(driver.findElement(By.xpath(filterByXpath))).sendKeys(accountNumber).sendKeys(Keys.ENTER).build().perform();
        log.info(String.format("Filtered %ss by Account Number: %s", entityName, accountNumber));

        wait.withTimeout(searchTimeout).until(ExpectedConditions.elementToBeClickable(By.xpath(rowXPath)));
        driver.findElement(By.xpath(rowXPath)).click();
        log.info(String.format("Selected %s row by Account Number: %s", entityName, accountNumber));
    }

    private void setAgentIdType(boolean isDebtor, String idType) {
        String entity = isDebtor ? DEBTOR : CREDITOR;
        String xpath = String.format(XP_INPUT_AGENT_ID_TYPE, entity);
        String suggXPath = String.format(XP_INPUT_SUGGESTION_BY_TEXT, idType);
        String entityName = isDebtor ? "DebtorAgent" : "CreditorAgent";

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        driver.findElement(By.xpath(xpath)).clear();
        driver.findElement(By.xpath(xpath)).sendKeys(idType);
        log.info(String.format("Set %s's ID Type: %s", entityName, idType));

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(suggXPath)));
        driver.findElement(By.xpath(suggXPath)).click();
        log.info(String.format("Selected suggested %s ID Type: %s", entityName, idType));
    }

    private void populateAgent(boolean isDebtor, String idType, String partyCode) {
        Duration openTimeout = Duration.ofSeconds(10);
        Duration searchTimeout = Duration.ofSeconds(5);
        boolean isBic = idType.trim().equalsIgnoreCase("bic");
        String valFieldName = isBic ? BIC_FIELD_NAME : BBAN_FIELD_NAME;
        String inputFieldName = isBic ? AGENT_INPUT_BIC : AGENT_INPUT_BBAN;
        String entity = isDebtor ? DEBTOR : CREDITOR;
        String threeDotsXPath = String.format(XP_INPUT_AGENT_BUTTON, entity, inputFieldName);
        String filterVal = isBic ? FILTER_BY_PARTY_CODE_BIC : FILTER_BY_PARTY_CODE_BBAN;
        String filterByXpath = String.format(XP_INPUT_FILTER_BY_PARTY_CODE, filterVal);
        String rowXPath = String.format(XP_ROW_BY_PARTY_CODE, filterVal, partyCode);
        String rowValXPath = rowXPath.concat(String.format(XP_ROW_VALUE_BY_PARTY_CODE, valFieldName));
        String inputXPath = String.format(XP_INPUT_AGENT_FIELD, entity, inputFieldName);
        String entityName = entity + "Agent";

        if (idType != null && !idType.isEmpty() && partyCode != null && !partyCode.isEmpty()) {
            setAgentIdType(isDebtor, idType);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(threeDotsXPath)));
            driver.findElement(By.xpath(threeDotsXPath)).click();
            log.info(String.format("Clicked %s's three-dots button...", entityName));

            wait.withTimeout(openTimeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterByXpath)));
            act.moveToElement(driver.findElement(By.xpath(filterByXpath))).sendKeys("").build().perform();
            driver.findElement(By.xpath(filterByXpath)).clear();
            act.moveToElement(driver.findElement(By.xpath(filterByXpath))).click().sendKeys(partyCode).sendKeys(Keys.ENTER).build().perform();
            log.info(String.format("Filtered %ss by Party Code: %s", entityName, partyCode));

            wait.withTimeout(searchTimeout).until(ExpectedConditions.elementToBeClickable(By.xpath(rowXPath)));
            // DEBUG
            act.moveToElement(driver.findElement(By.xpath(rowXPath))).sendKeys("").build().perform();
            screenshotTaker.screenshot(String.format("populate_%s", entityName));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(rowValXPath)));
            String expectedValue = driver.findElement(By.xpath(rowValXPath)).getText().trim();
            driver.findElement(By.xpath(rowXPath)).click();
            log.info(String.format("Selected %s row by Party Code: %s", entityName, partyCode));

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(inputXPath)));
            // Check input success
            String txt = driver.findElement(By.xpath(inputXPath)).getAttribute("value").trim();
            log.info(String.format("Field value - expected '%s' / actual '%s'", expectedValue, txt));
        }
    }
}
