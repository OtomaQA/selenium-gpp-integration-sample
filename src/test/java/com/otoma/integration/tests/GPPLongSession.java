package com.otoma.integration.tests;

import com.otoma.integration.gpp.LoginHandler;
import com.otoma.integration.gpp.MessageActionHandler;
import com.otoma.integration.gpp.SearchHandler;
import com.otoma.integration.util.Util;
import com.otoma.scripting.OtomaScriptingContext;
import com.otoma.scripting.OtomaSelenium;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;

public class GPPLongSession extends OtomaSelenium {
    private final static String TAG_USER_ID = "UsrId";
    private final static String TAG_USER_PASS = "UsrPass";
    private final static String TAG_MID = "PmtLclRef";
    private final static String TAG_RATE_ACTION = "RateActn";
    private final static String TAG_USER_ACTION = "UsrActn";
    private final static String TAG_APP_URL = "applicationUrl";

    public String applicationUrl = "";
    public String loginUsername = "";
    public String loginPassword = "";
    public String targetMID = "";
    public String approveRate = "";
    public String messageAction = "";

    private static LoginHandler login;
    private static SearchHandler search;
    private static MessageActionHandler msg;
    private static Util util;

    private static String mainWindowHandle;
    private static final Logger log = Logger.getLogger(GPPLongSession.class.getName());

    @BeforeEach
    public void initTest() {
        login = new LoginHandler(selenium, wait, this);
        search = new SearchHandler(selenium, wait, this);
        msg = new MessageActionHandler(selenium, wait, this);
        util = new Util(selenium, wait);
    }

    @Test
    public void teardown() {
        boolean loggedOut = login.tryToLogout();
        assertTrue(loggedOut);
        String msg = loggedOut ? "Logged-out" : "Failed to logout";
        log.info(msg);
    }

    @Test
    public void getRate() {
        try {
            applicationUrl = OtomaScriptingContext.getConfiguration(TAG_APP_URL);
            loginUsername = OtomaScriptingContext.getData(TAG_USER_ID);
            loginPassword = OtomaScriptingContext.getData(TAG_USER_PASS);
            targetMID = OtomaScriptingContext.getData(TAG_MID);

            mainWindowHandle = selenium.getWindowHandle();
            log.info("Parent window handle: " + mainWindowHandle);
            selenium.get(applicationUrl);
            boolean loggedIn = login.tryToLogin(loginUsername, loginPassword);
            assertTrue(loggedIn);

            boolean searchTest = search.search(targetMID);
            assertTrue(searchTest);

            int resultIndex = 1;
            boolean openResultTest = search.openSearchResultByIndex(resultIndex, mainWindowHandle);
            assertTrue(openResultTest);

            boolean gotRate = msg.getRate();
            assertTrue(gotRate);
            int winCount = selenium.getWindowHandles().size();
            log.log(Level.INFO, String.format("WinCount = %d", winCount));
        }
        catch (Exception e) {
            log.log(Level.WARNING, "Error occurred", e);
            screenshot("getRate_Error");
        }
    }

    @Test
    public void rateAction() {
        try {
            approveRate = OtomaScriptingContext.getData(TAG_RATE_ACTION);
            messageAction = OtomaScriptingContext.getData(TAG_USER_ACTION);

            boolean isApproveRate = Boolean.valueOf(approveRate);
            boolean rateAct = msg.rateAction(isApproveRate);
            assertTrue(rateAct);

            boolean populatedNote = msg.populateMessageNotes(msg.generateMessageNote(""));
            assertTrue(populatedNote);

            boolean performedAction = msg.clickActionButton(messageAction, false);
            assertTrue(performedAction);

            String strRateAct = rateAct ? "APPROVE" : "REJECT";
            log.info(String.format("Performed Rate Action (%s): %b | Added note: %b | Performed Message Action (%s): %b", strRateAct, rateAct, populatedNote, messageAction, performedAction));
        }
        catch (Exception e) {
            log.log(Level.WARNING, "Error occurred", e);
            screenshot("rateAct_Error");
        }
    }
}
