package com.otoma.integration.tests;

import com.otoma.integration.gpp.LoginHandler;
import com.otoma.integration.gpp.MessageActionHandler;
import com.otoma.integration.gpp.PaymentHandler;
import com.otoma.integration.gpp.SearchHandler;
import com.otoma.integration.util.Util;
import com.otoma.scripting.OtomaScriptingContext;
import com.otoma.scripting.OtomaSelenium;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GPPPaymentCreation extends OtomaSelenium {
    private final static String TAG_USER_ID = "UsrId";
    private final static String TAG_USER_PASS = "UsrPass";
    private final static String TAG_MID = "PmtLclRef";
    private final static String TAG_RATE_ACTION = "RateActn";
    private final static String TAG_USER_ACTION = "UsrActn";
    private final static String TAG_APP_URL = "applicationUrl";
    // Payment tags
    //TODO: Get relevant Element Field Tags from Otoma (temp values)
    private final static String TAG_PAYMENT_TYPE = "PaymentType";
    private final static String TAG_PAYMENT_DPT = "PaymentDepartment";
    private final static String TAG_PAYMENT_OFC = "PaymentOffice";
    private final static String TAG_PAYMENT_INSTRID = "PaymentInstructionId";
    private final static String TAG_PAYMENT_CCY = "PaymentCurrency";
    private final static String TAG_PAYMENT_AMT = "PaymentAmount";
    private final static String TAG_PAYMENT_VALDT = "PaymentValueDate";
    private final static String TAG_PAYMENT_CHRG_BR = "PaymentChargesBearer";
    private final static String TAG_PAYMENT_DBT_ACT = "PaymentDebtorAccount";
    private final static String TAG_PAYMENT_DBT_AGT_ID_TYPE = "PaymentDebtorAgentIDType";
    private final static String TAG_PAYMENT_DBT_AGT_PTY = "PaymentDebtorAgentPartyCode";
    private final static String TAG_PAYMENT_CDT_ACT = "PaymentCreditorAccount";
    private final static String TAG_PAYMENT_CDT_AGT_ID_TYPE = "PaymentCreditorAgentIDType";
    private final static String TAG_PAYMENT_CDT_AGT_PATY = "PaymentCreditorAgentPartyCode";

    public String applicationUrl = "";
    public String loginUsername = "";
    public String loginPassword = "";
    public String targetMID = "";
    public String approveRate = "";
    public String messageAction = "";

    private static LoginHandler login;
    private static SearchHandler search;
    private static MessageActionHandler msg;
    private static PaymentHandler payment;
    private static Util util;

    private static String mainWindowHandle;
    private static final Logger log = Logger.getLogger(GPPPaymentCreation.class.getName());

    @BeforeEach
    public void initTest() {
        login = new LoginHandler(selenium, wait, this);
        search = new SearchHandler(selenium, wait, this);
        msg = new MessageActionHandler(selenium, wait, this);
        payment = new PaymentHandler(selenium, wait, this);
        util = new Util(selenium, wait);
    }

    @Test
    public void teardown() {
        boolean loggedOut = login.tryToLogout();
        assertTrue(loggedOut);
        String msg = loggedOut ? "Logged-out" : "Failed to logout";
        log.info(msg);
    }

    @Test
    public void createPayment() {
        boolean saveChangesOnError = false;
        try {
            applicationUrl = OtomaScriptingContext.getConfiguration(TAG_APP_URL);
            loginUsername = OtomaScriptingContext.getData(TAG_USER_ID);
            loginPassword = OtomaScriptingContext.getData(TAG_USER_PASS);

            //Payment Data
            String paymentType = OtomaScriptingContext.getData(TAG_PAYMENT_TYPE);
            String department = OtomaScriptingContext.getData(TAG_PAYMENT_DPT);
            String office = OtomaScriptingContext.getData(TAG_PAYMENT_OFC);
            String instructionId = OtomaScriptingContext.getData(TAG_PAYMENT_INSTRID);
            String currency = OtomaScriptingContext.getData(TAG_PAYMENT_CCY);
            String amount = OtomaScriptingContext.getData(TAG_PAYMENT_AMT);
            String valueDate = OtomaScriptingContext.getData(TAG_PAYMENT_VALDT);
            String chargesBearer = OtomaScriptingContext.getData(TAG_PAYMENT_CHRG_BR);
            String debtorAccount = OtomaScriptingContext.getData(TAG_PAYMENT_DBT_ACT);
            String debtorAgentIdType = OtomaScriptingContext.getData(TAG_PAYMENT_DBT_AGT_ID_TYPE);
            String debtorAgentPartyCode = OtomaScriptingContext.getData(TAG_PAYMENT_DBT_AGT_PTY);
            String creditorAccount = OtomaScriptingContext.getData(TAG_PAYMENT_CDT_ACT);
            String creditorAgentIdType = OtomaScriptingContext.getData(TAG_PAYMENT_CDT_AGT_ID_TYPE);
            String creditorAgentPartyCode = OtomaScriptingContext.getData(TAG_PAYMENT_CDT_AGT_PATY);

            mainWindowHandle = selenium.getWindowHandle();
            log.info("Parent window handle: " + mainWindowHandle);
            selenium.get(applicationUrl);
            boolean loggedIn = login.tryToLogin(loginUsername, loginPassword);
            assertTrue(loggedIn);

            boolean paymentCreated = payment.createNewPayment(paymentType, department, office, instructionId, currency, amount, valueDate, chargesBearer, debtorAccount,
                    debtorAgentIdType, debtorAgentPartyCode, creditorAccount, creditorAgentIdType, creditorAgentPartyCode);
            assertTrue(paymentCreated);

        } catch (Exception e) {
            log.log(Level.WARNING, "Error occurred", e);
            screenshot("createPayment_Error");
            payment.closePaymentScreen(saveChangesOnError);
        // TODO: TEMP
        } finally {
            teardown();
        }
    }

    @Test
    public void getRate() {
        try {
            applicationUrl = OtomaScriptingContext.getConfiguration(TAG_APP_URL);
            loginUsername = OtomaScriptingContext.getData(TAG_USER_ID);
            loginPassword = OtomaScriptingContext.getData(TAG_USER_PASS);
            targetMID = OtomaScriptingContext.getData(TAG_MID);

            mainWindowHandle = selenium.getWindowHandle();
            log.info("Parent window handle: " + mainWindowHandle);
            selenium.get(applicationUrl);
            boolean loggedIn = login.tryToLogin(loginUsername, loginPassword);
            assertTrue(loggedIn);

            boolean searchTest = search.search(targetMID);
            assertTrue(searchTest);

            int resultIndex = 1;
            boolean openResultTest = search.openSearchResultByIndex(resultIndex, mainWindowHandle);
            assertTrue(openResultTest);

            boolean gotRate = msg.getRate();
            assertTrue(gotRate);
            int winCount = selenium.getWindowHandles().size();
            log.log(Level.INFO, String.format("WinCount = %d", winCount));
        }
        catch (Exception e) {
            log.log(Level.WARNING, "Error occurred", e);
            screenshot("getRate_Error");
        }
    }

    @Test
    public void rateAction() {
        try {
            approveRate = OtomaScriptingContext.getData(TAG_RATE_ACTION);
            messageAction = OtomaScriptingContext.getData(TAG_USER_ACTION);

            boolean isApproveRate = Boolean.valueOf(approveRate);
            boolean rateAct = msg.rateAction(isApproveRate);
            assertTrue(rateAct);

            boolean populatedNote = msg.populateMessageNotes(msg.generateMessageNote(""));
            assertTrue(populatedNote);

            boolean performedAction = msg.clickActionButton(messageAction, false);
            assertTrue(performedAction);

            String strRateAct = rateAct ? "APPROVE" : "REJECT";
            log.info(String.format("Performed Rate Action (%s): %b | Added note: %b | Performed Message Action (%s): %b", strRateAct, rateAct, populatedNote, messageAction, performedAction));
        }
        catch (Exception e) {
            log.log(Level.WARNING, "Error occurred", e);
            screenshot("rateAct_Error");
        }
    }
}
