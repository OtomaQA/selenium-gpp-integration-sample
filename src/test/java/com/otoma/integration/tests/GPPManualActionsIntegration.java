package com.otoma.integration.tests;

import com.otoma.integration.gpp.LoginHandler;
import com.otoma.integration.gpp.MessageActionHandler;
import com.otoma.integration.gpp.SearchHandler;
import com.otoma.integration.util.Util;
import com.otoma.scripting.OtomaScriptingContext;
import com.otoma.scripting.OtomaSelenium;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;

public class GPPManualActionsIntegration extends OtomaSelenium
{
    private final static String TAG_USER_ID = "UsrId";
    private final static String TAG_USER_PASS = "UsrPass";
    private final static String TAG_MID = "PmtLclRef";
    private final static String TAG_USER_ACTION = "UsrActn";
    private final static String TAG_APP_URL = "applicationUrl";

    public String targetURL = "";
    public String loginUsername = "";
    public String loginPassword = "";
    public String targetMID = "";
    public String messageAction = "";

    private static LoginHandler login;
    private static SearchHandler search;
    private static MessageActionHandler msg;
    private static Util util;

    private static String mainWindowHandle;
    private static final Logger log = Logger.getLogger(GPPManualActionsIntegration.class.getName());

    private void switchToParentWindow() {
        if (mainWindowHandle != null && mainWindowHandle.length() > 0) {
            util.trySwitchToWindow(mainWindowHandle);
        }
    }

    @BeforeEach
    public void preTest() {
        login = new LoginHandler(selenium, wait, this);
        search = new SearchHandler(selenium, wait, this);
        msg = new MessageActionHandler(selenium, wait, this);
        util = new Util(selenium, wait);
    }

    // Each Test Class may include a Test method named 'teardown' -
    // Will be automatically executed at the end of each Selenium session
    @AfterEach
    public void finish() {
        switchToParentWindow();
        boolean loggedOut = login.tryToLogout();
        String msg = loggedOut ? "Logged-out" : "Failed to logout";
        log.info(msg);
    }

    @Test
    public void actionOnPayment() {
        try {
            targetURL = OtomaScriptingContext.getConfiguration(TAG_APP_URL);
            loginUsername = OtomaScriptingContext.getData(TAG_USER_ID);
            loginPassword = OtomaScriptingContext.getData(TAG_USER_PASS);
            targetMID = OtomaScriptingContext.getData(TAG_MID);
            messageAction = OtomaScriptingContext.getData(TAG_USER_ACTION);

            log.info(String.format("Working on: \n%s\n%s | %s\n%s -> %s", targetURL, loginUsername, loginPassword, targetMID, messageAction));

            selenium.get(targetURL);
            selenium.manage().window().maximize();
            mainWindowHandle = selenium.getWindowHandle();
            log.info("Parent window handle: " + mainWindowHandle);

            boolean loggedIn = login.tryToLogin(loginUsername, loginPassword);
            assertTrue(loggedIn);

            boolean searchTest = search.search(targetMID);
            assertTrue(searchTest);

            int resultIndex = 1;
            boolean openResultTest = search.openSearchResultByIndex(resultIndex, mainWindowHandle);
            assertTrue(openResultTest);

            boolean populatedNote = msg.populateMessageNotes(msg.generateMessageNote(""));
            assertTrue(populatedNote);

            boolean performedAction = msg.clickActionButton(messageAction, false);
            assertTrue(performedAction);

            switchToParentWindow();

        } catch (Exception e) {
            log.log(Level.WARNING, "Error occurred", e);
            screenshot("TestError");
            assertTrue(false);
        }
    }
}
