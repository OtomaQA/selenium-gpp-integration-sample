package com.otoma.scripting;

public interface ScreenshotTaker {
    void screenshot(String name);
}
