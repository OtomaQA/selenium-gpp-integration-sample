package com.otoma.scripting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class OtomaScriptingContext {
    private static Properties inputs;

    public static String getConfiguration(String key) {
        return get("configuration." + key);
    }

    public static String getData(String key) {
        return get("data." + key);
    }

    private static String get(String key) {
        return getProperties().getProperty(key);
    }

    static Properties getProperties() {
        if (inputs == null) {
            inputs = loadProperties("input.properties");
        }
        return inputs;
    }

    static Properties loadProperties(String filename) {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream(filename)) {
            properties.load(in);
        } catch (IOException e) {
            System.err.printf("No '%s' file found in working directory (%s).\n" +
                    "Otoma scripting context will be empty.", filename, getWorkingDirectory());
        }
        return properties;
    }

    private static String getWorkingDirectory() {
        File workingDirectory = new File(".");
        try {
            return workingDirectory.getCanonicalPath();
        } catch (IOException e) {
            return workingDirectory.getAbsolutePath();
        }
    }
}
