package com.otoma.scripting;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import static java.lang.String.format;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Collections.singletonMap;

public abstract class OtomaSelenium implements ScreenshotTaker {
    private final static int WAIT_TIMEOUT_SEC = 15;
    protected RemoteWebDriver selenium;
    protected WebDriverWait wait;


    public void screenshot(String name) {
        try {
            Path screenshot = selenium.getScreenshotAs(OutputType.FILE).toPath();
            Files.move(screenshot, Paths.get(name + ".png"), REPLACE_EXISTING);
        }
        catch (IOException e) {
            throw new ScreenshotException(e);
        }
    }

    public class ScreenshotException extends RuntimeException {
        public ScreenshotException(Throwable cause) {
            super("Failed to take screenshot", cause);
        }
    }

    protected String getSessionName() {
        return getClass().getName();
    }

    @BeforeEach
    public final void otomaSeleniumSetup() throws Exception {
        String webDriverUrl = OtomaScriptingContext.getConfiguration("webdriver-url");
        String sessionId = OtomaScriptingContext.getConfiguration("selenium-session-id." + getSessionName());
        this.selenium = new ReconnectingWebDriver(new URL(webDriverUrl), sessionId);
        this.selenium.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        this.wait = new WebDriverWait(this.selenium, WAIT_TIMEOUT_SEC);
    }

    @AfterEach
    public final void otomaSeleniumTeardown() {
        grabAfterExecutionScreenshot();
        saveSession();
    }

    private void saveSession() {
        if (selenium == null || selenium.getSessionId() == null) {
            return;
        }
        String filename = "session.properties";
        String sessionName = getSessionName();
        String sessionId = selenium.getSessionId().toString();
        Properties sessionProperties = new Properties();
        sessionProperties.setProperty("name", sessionName);
        sessionProperties.setProperty("id", sessionId);
        writeProperties(filename, sessionProperties);
        if ("true".equals(OtomaScriptingContext.getConfiguration("local-dev-mode"))) {
            Properties inputProperties = OtomaScriptingContext.getProperties();
            inputProperties.setProperty(
                    "configuration.selenium-session-id." + getSessionName(),
                    sessionId
            );
            writeProperties("input.properties", inputProperties);
        }
    }

    private void writeProperties(String filename, Properties properties) {
        try (Writer writer = new FileWriter(filename)) {
            properties.store(writer, null);
        } catch (IOException ex) {
            String msg = format("Failed to write properties to [%s]: %s", filename, properties);
            throw new RuntimeException(msg, ex);
        }
    }

    private void grabAfterExecutionScreenshot() {
        try {
            screenshot("afterExecution");
        } catch (Exception ex) {
            System.err.println("Error while getting afterExecution screenshot: " + ex);
        }
    }
}

class ReconnectingWebDriver extends RemoteWebDriver {
    public ReconnectingWebDriver(URL url, String sessionId) {
        super(url, desiredCapabilities());
        if (sessionId != null) {
            quit();
            setSessionId(sessionId);
        }
    }

    private static Capabilities desiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities(BrowserType.CHROME, "", Platform.ANY);
        capabilities.setAcceptInsecureCerts(true);
        capabilities.setCapability("goog:chromeOptions", singletonMap(
                "args", Arrays.asList(
                        "--no-sandbox",
                        "--disable-dev-shm-usage",
                        "--disable-gpu",
                        "--enable-chrome-logs",
                        "--readable-timestamp",
                        "--append-log",
                        "--log-level=ALL"
                )));
        return capabilities;
    }
}
